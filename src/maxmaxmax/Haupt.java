package maxmaxmax;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Haupt {
	public static void main(String[] args)
	{
		Connection conn = null;
		try 
		{
			Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();

			conn = DriverManager.getConnection("jdbc:mysql://localhost/myanimelist?user=root&password=");
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM anime");
			
			while ( rs.next() ) 
			{
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4) + " Folgen");
				System.out.println("Status "+rs.getString(5));
				System.out.println("TypID "+rs.getString(6));
				System.out.println("Ersheinungsdatum: "+rs.getString(7));
				System.out.println("----------------------------------");
			} 

			
			stmt.executeUpdate("insert into anime values (31,'Es Funktioniert', 'Yaay <3', '25', '1', '1', '2011-06-10 06:15:50')");
			
			rs = stmt.executeQuery("SELECT * FROM anime");
			
			while ( rs.next() )
			{
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4) + " Folgen");
				System.out.println("Status "+rs.getString(5));
				System.out.println("TypID "+rs.getString(6));
				System.out.println("Ersheinungsdatum: "+rs.getString(7));
				System.out.println("----------------------------------");
			}
			
			stmt.executeUpdate("delete from anime where Anime_ID = 31");
			
			rs = stmt.executeQuery("SELECT * FROM anime");
			
			while ( rs.next() )
			{
				//System.out.printf( "%s, %s, %s %n", rs.getString(1),
						//rs.getString(2), rs.getString(3));
				System.out.println(rs.getString(1));
				System.out.println(rs.getString(2));
				System.out.println(rs.getString(3));
				System.out.println(rs.getString(4) + " Folgen");
				System.out.println("Status "+rs.getString(5));
				System.out.println("TypID "+rs.getString(6));
				System.out.println("Ersheinungsdatum: "+rs.getString(7));
				System.out.println("----------------------------------");
			}
			rs.close();

			stmt.close();

		} catch (Exception e) {
			// For the sake of this tutorial, let's keep exception handling simple
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
			}
		}
	}
}
